<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Material Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/style.default.css" id="theme-stylesheet">
      <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
      <script src="/views/js/func.js"></script>
  </head>
  <script type="text/javascript">
      // let token = localStorage.getItem('token')
      // console.log(token)
      // if (token)
      // {
      //     //console.log("heere");
      //     //window.location.href='/view'
      // }

       function login1() {
      //     method = "POST"
      //     datatype = "json"
      //     url = "/login"
      //     data = $('#loginFrom').serialize()
      //     headers = {}
      //
      //     //console.log(url)
      //
      //
      //     res = sendAjax(method, datatype, data, url, headers)
      //
      //     //console.log(res)
      //    //   console.log(res.status)
      //     if (res.status == 200)
      //     {
      //
      //         alert("SUCCESS");
      //         localStorage.setItem("token", result.token)
      //         localStorage.setItem("id", result.id)
      //         localStorage.setItem("email", result.email)
      //         localStorage.setItem("birthday", result.birthday)
      //         localStorage.setItem("name", result.name)
      //         window.location.href='/view/'
      //     }
      //     else if (res.status == 201)
      //     {
      //         alert(result.status + "  "  + result.errMessage);
      //         let verifyEmail = `<div class="content"><a href="/verify/?token=${result.verify_token}">please click and confirm your email</div>`
      //         localStorage.setItem("verifyToken", result.verify_token)
      //         // localStorage.setItem('verifyEmail', verifyEmail)
      //         // document.getElementById('master').innerHTML = verifyEmail
      //         window.location.href='/vm'
      //     }
      //     else
      //     {
      //         alert("failed!  " + res.status + "  "  + res.errMessage);
      //     }


          $.ajax({

              type: "POST",//http method
              dataType: "json",//data type
              url: "/login" ,//url
              data: $('#loginFrom').serialize(),
              success: function (result) {
                  //console.log(result);//for debug
                  if (result.status == 200) {
                      alert("SUCCESS");
                      localStorage.setItem("token", result.token)
                      localStorage.setItem("id", result.id)
                      localStorage.setItem("email", result.email)
                      localStorage.setItem("birthday", result.birthday)
                      localStorage.setItem("name", result.name)
                      window.location.href='/view/'

                      // let content = `<div class="content"><div class="form-group">NAME: ${result.name}</div><div class="form-group">EMAIL: ${result.email}</div><div class="form-group">BIRTHDAY: ${result.birthday}</div><div class="form-group"><a href='/update'>update</a></div><div class="form-group"><button id="login" type="button" onclick="logout()" class="btn btn-primary">Logout</button></div></div>`
                      // localStorage.setItem("content", content)
                      // document.getElementById('master').innerHTML = content

                  }
                  else if (result.status == 201)
                  {
                      alert(result.status + "  "  + result.errMessage);
                      let verifyEmail = `<div class="content"><a href="/verify/?token=${result.verify_token}">please click and confirm your email</div>`
                      localStorage.setItem("verifyToken", result.verify_token)
                      // localStorage.setItem('verifyEmail', verifyEmail)
                      // document.getElementById('master').innerHTML = verifyEmail
                      window.location.href='/vm'
                  }
                  else
                  {
                      alert("failed!  " + result.status + "  "  + result.errMessage);
                  }

              },
              error : function() {
                  alert("system error！");
              }
          });

      }
  </script>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>WELCOME</h1>
                  </div>
                  <p></p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white" >
              <div class="form d-flex align-items-center" id="master">
                  <div class="content" >
                      <form name="loginFrom"  method="post" action="" class="form-validate" id="loginFrom">
                          <div class="form-group">
                              <input id="login-username" type="text" name="email" required data-msg="Please input your Email" placeholder="Email"  class="input-material">
                          </div>
                          <div class="form-group">
                              <input id="login-password" type="password" name="pass" required data-msg="Please input your password" placeholder="Password" class="input-material">
                          </div>
                          <button id="login" type="button" onclick="login1()" class="btn btn-primary">Login</button>
                          <div style="margin-top: -40px;">

                              <div class="custom-control custom-checkbox " style="float: right;">

                              </div>
                              <div class="custom-control custom-checkbox " style="float: right;">
                              </div>
                          </div>
                      </form>
                      <br />
                      <small>Create an account?</small><a href="reg" class="signup">&nbsp;Signin</a>
                  </div>



              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
<!--    <script src="https://libs.baidu.com/jquery/1.10.2/jquery.min.js"></script>-->
<!--    <script src="https://ajax.aspnetcdn.com/ajax/bootstrap/4.2.1/bootstrap.min.js"></script>-->
<!--    <script src="views/vendor/jquery-validation/jquery.validate.min.js"></script><!--form check-->-->
    <!-- Main File-->
    <script src="views/js/front.js"></script>

  </body>

</html>