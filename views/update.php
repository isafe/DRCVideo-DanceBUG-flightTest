<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Material Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="views/css/style.default.css" id="theme-stylesheet">
      <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
      <script src="/views/js/func.js"></script>
      <script language="JavaScript">
          window.onload = function(){
              strYYYY = document.form1.YYYY.outerHTML;
              strMM = document.form1.MM.outerHTML;
              strDD = document.form1.DD.outerHTML;
              MonHead = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

//YEAR select
              var y = new Date().getFullYear();
              var str = strYYYY.substring(0, strYYYY.length - 9);
              for (var i = (y-30); i < (y+30); i++) //以今年为准，前30年，后30年
              {
                  str += "<option value='" + i + "'> " + i + "</option>\r\n";
              }
              document.form1.YYYY.outerHTML = str +"</select>";

//month select
              var str = strMM.substring(0, strMM.length - 9);
              for (var i = 1; i < 13; i++)
              {
                  str += "<option value='" + i + "'> " + i + "</option>\r\n";
              }
              document.form1.MM.outerHTML = str +"</select>";

              document.form1.YYYY.value = y;
              document.form1.MM.value = new Date().getMonth() + 1;
              var n = MonHead[new Date().getMonth()];
              if (new Date().getMonth() ==1 && IsPinYear(YYYYvalue)) n++;
              writeDay(n); //赋日期下拉框
              document.form1.DD.value = new Date().getDate();
          }
          function YYYYMM(str) //figure out leap year
          {
              var MMvalue = document.form1.MM.options[document.form1.MM.selectedIndex].value;
              if (MMvalue == ""){DD.outerHTML = strDD; return;}
              var n = MonHead[MMvalue - 1];
              if (MMvalue ==2 && IsPinYear(str)) n++;
              writeDay(n)
          }
          function MMDD(str) //月发生变化时日期联动
          {
              var YYYYvalue = document.form1.YYYY.options[document.form1.YYYY.selectedIndex].value;
              if (str == ""){DD.outerHTML = strDD; return;}
              var n = MonHead[str - 1];
              if (str ==2 && IsPinYear(YYYYvalue)) n++;
              writeDay(n)
          }
          function writeDay(n) //据条件写日期的下拉框
          {
              var s = strDD.substring(0, strDD.length - 9);
              for (var i=1; i<(n+1); i++)
                  s += "<option value='" + i + "'> " + i + "</option>\r\n";
              document.form1.DD.outerHTML = s +"</select>";
          }
          function IsPinYear(year)//if leap year
          { return(0 == year%4 && (year%100 !=0 || year%400 == 0))}
          //-->




      </script>
  </head>
  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>WELCOME</h1>
                  </div>
                  <p></p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white" >
              <div class="form d-flex align-items-center" id="master">
                  <div class="content" >
                      <form name="form1"  method="post" action="" class="form-validate" id="form1">
                          <div class="form-group">
                              <input id="email" type="text" name="email"  placeholder="email"  class="input-material" readonly="readonly">
                          </div>

                              <div class="form-group">
                                  <input id="name" type="text" name="name"  placeholder="name"  class="input-material">
                              </div>

                                  <div class="form-group">
                                      <input id="" type="password" name="pass"  placeholder="Password"  class="input-material">
                                  </div>

                              <div class="form-group">
                                      <input id="" type="password" name="pass2"  placeholder="confirmed Password" class="input-material">
                              </div>
                              <div class="form-group">


                                      <select name=YYYY onchange="YYYYMM(this.value)">
                                          <option value="">YEAR</option>
                                      </select>
                                      <select name=MM onchange="MMDD(this.value)">
                                          <option value="">MONTH</option>
                                      </select>
                                      <select name=DD>
                                          <option value="">DAY</option>
                                      </select>
                              </div>


                                    <button id="login" type="button" onclick="login1()" class="btn btn-primary">Update</button>
                                    <button id="login" type="button" onclick="history.back(-1)" class="btn btn-primary">Back</button>
                                    <button id="login" type="button" onclick="logout()" class="btn btn-primary">Logout</button>
                                      <div style="margin-top: -40px;">

                                          <div class="custom-control custom-checkbox " style="float: right;">

                                          </div>
                                          <div class="custom-control custom-checkbox " style="float: right;">
                                          </div>
                                      </div>
                      </form>
                      <div class="form-group">

                      </div>

                  </div>



              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="views/js/front.js"></script>

  </body>
  <script type="text/javascript">

      let token = localStorage.getItem('token')
      let email = localStorage.getItem('email')
      let name = localStorage.getItem('name')
      let birthday = localStorage.getItem('birthday')
      let id = localStorage.getItem('id')

      if (!token)
      {
          alert("token is empty")
          window.location.href='/'
      }
      else
      {
          document.getElementById('name').value = name
          document.getElementById('email').value = email

          function login1() {
              $.ajax({

                  type: "POST",//http method
                  dataType: "json",//data type
                  url: "/update/"+id ,//url
                  data: $('#form1').serialize(),
                  headers:{
                      'Authorization': keyName+token
                  },
                  success: function (result) {
                      console.log(result);//for debug
                      if (result.status == 200) {
                          alert("SUCCESS");
                          window.location.href='/view/'

                      }
                      else
                      {
                          alert("failed!  " + result.status + "  "  + result.errMessage);
                      }

                  },
                  error : function() {
                      alert("system error！");
                  }
              });

          }
      }





  </script>
</html>