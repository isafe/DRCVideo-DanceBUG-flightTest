<?php

function db()
{
    $host = "localhost";
    $username = "root";
    $password = "neo123$%^";
    $database = "flighttest";
    $conn = mysqli_connect($host, $username, $password, $database);

    if (mysqli_connect_errno($conn))
    {
        return mysqli_connect_error();
    }
    else
    {
        mysqli_query($conn, "set name utf8");
        return $conn;
    }


}


function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function checkbirthday($month, $day, $year)
{
    $min_age = 0;
    $max_age = 200;


    // verify valid date
    if (! checkdate ( $month, $day, $year )) {
        return false;
    }

    // take current year, month, day
    list ( $this_year, $this_month, $this_day ) = explode ( ',', date ( 'Y, m, d' ) );
    $min_year = $this_year - $max_age;
    $max_year = $this_year - $min_age;

//    print "$min_year,$this_month,$this_day\n<br/>";
//    print "$max_year,$this_month,$this_day\n<br/>";
//    print "$year,$month,$day\n<br/>";

    if (($year > $min_year) && ($year < $max_year))
    { // year between max and min
        return true;
    } elseif (($year == $max_year) && (($month < $this_month) || (($month == $this_month && ($day < $this_day)))))
    {
        return true;
    } elseif (($year == $min_year) && (($month > $this_month) || (($month == $this_month && ($day > $this_day)))))
    {
        return true;
    } else {
        return false;
    }
}

function str_random($length) // make email verify token
{
    $chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
        'i', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r', 's',
        't', 'u', 'v', 'w', 'x', 'y','z', 'A', 'B', 'C', 'D',
        'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O',
        'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
    $keys = array_rand($chars, $length);
    $data = '';
    for($i = 0; $i < $length; $i++)
    {
        $data .= $chars[$keys[$i]];
    }
    return $data;
}

function getheaders()
{
    $headers = array ();
    foreach ($_SERVER as $name => $value)
    {
        if (substr($name, 0, 5) == 'HTTP_')
        {
            $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
        }
    }
    return $headers;
}


//function sendMail($to, $title, $content)
//{
//
//
//}
