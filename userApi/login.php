<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once './helper/conn.php';
require_once './helper/jwt.php';
require_once './helper/phpmailer/src/PHPMailer.php';
require_once './helper/phpmailer/src/SMTP.php';
require_once './helper/phpmailer/src/Exception.php';


function login()
{
    $status = $errMessage = ''; //set variable of status and error message null
    $email = Flight::request()->data['email'];
    $pass = Flight::request()->data['pass'];


    if (empty($email)) //check email if empty
    {
        $errMessage =  "Email is empty, please input it";
        $status = '404';
    }
    elseif (empty($pass))// check password if empty
    {
        $errMessage = "pass is empty, please input it!";
        $status = '404';
    }
    else // password and email are not empty, then select user
    {
        $sql = "select * from users where email = '".$email."' limit 1";
        $result = mysqli_query(db(), $sql);
        if ($row = mysqli_fetch_array($result)) // this email user exist
        {
            // check user's password if match
            if (password_verify($pass, $row['password'])) //if passwords match
            {
                //check email if confirmed
                if (($row['verified']) == 0) //email does not confirm, return verify token and message
                {


                    $mail = new PHPMailer(true);

                    try {
                        //服务器配置
                        $mail->CharSet ="UTF-8";                     //set email charset
                        $mail->SMTPDebug = 0;                        // set debug input false
                        $mail->isSMTP();                             // use SMTP
                        $mail->Host = 'smtp.gmail.com';                // SMTP server
                        $mail->SMTPAuth = true;                      // allow STMP auth
                        $mail->Username = 'flighttest2020@gmail.com';                // SMTP username = email account
                        $mail->Password = 'neo123$%^';             // SMTP password
                        $mail->SMTPSecure = 'ssl';                    // allow TLS or ssl protocol
                        $mail->Port = 465;                            // server port: 25 or 465, gmail use port 465

                        $mail->setFrom('flighttest2020@gmail.com', 'Admin');  //sender's email address and name
                        $mail->addAddress($row['email'], $row['name']);  // to email address and name
                        //$mail->addAddress('ellen@example.com');  // more email address
                        $mail->addReplyTo('flighttest2020@gmail.com', 'Admin'); //replay to email address, usually same as sender
                        //$mail->addCC('cc@example.com');                    //cc
                        //$mail->addBCC('bcc@example.com');                    //bcc

                        //attached file
                        // $mail->addAttachment('../xy.zip');         // add file
                        // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // send file and rename

                        //Content
                        $mail->isHTML(true);                                  // if use HTML format
                        $mail->Subject = 'email confirm link' . time();
                        $mail->Body    = '<h1><a href=www.freeshow.download/vm/?verifyToken='.$row['verify_token'].'>confirm</a> Please</h1>'. date('Y-m-d H:i:s');
                        $mail->AltBody = 'www.freeshow.donwload/vm'.$row['verify_token'];

                        $mail->send();

                        $errEmail = 'email send success!';
                    } catch (Exception $e) {
                        $errEmail =  'email send failed: '.$mail->ErrorInfo;
                    }
                    $status = '201';
                    $errMessage = 'email need confirm!';
                    $res = array(
                        'status' => $status,
                        'errMessage' => $errMessage,
                        'verify_token' => $row['verify_token'],
                        'errEmail' => $errEmail
                    );

                }
                else // email confirmed and return success and token of JWT
                {
                    $payload=array('sub'=>'Flight','name'=>$row['email'],'iat'=>1516239022);
                    $jwt=new Jwt;
                    $token=$jwt->getToken($payload);
                    $status = '200';
                    $res = array(
                        'id' => $row['id'],
                        'email' =>$row['email'],
                        'birthday' => $row['birthday'],
                        'name' => $row['name'],
                        'token' => $token,
                        'status' => $status,
                    );
                }

                mysqli_close(db()); //close database



            }
            else // password and email do not match
            {
                $errMessage = 'password and email not match';
                $status = '500';
            }


        }else // email address not in database
        {
            $status = "404";
            $errMessage = 'email not exist';
        }



    }

    if ($status <> '200' && $status <> '201') // error status, and return error message and status
    {
        $res = array(
            'errMessage' => $errMessage,
            'status' => $status
        );
    }




    return $res;

}