<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require_once './helper/conn.php';
require_once './helper/jwt.php';
require_once './helper/phpmailer/src/PHPMailer.php';
require_once './helper/phpmailer/src/SMTP.php';
require_once './helper/phpmailer/src/Exception.php';

function reg()
{
    $name = Flight::request()->data['name'];
    $pass = Flight::request()->data['pass'];
    $pass2 = Flight::request()->data['pass2'];
    $email = Flight::request()->data['email'];
    $yyyy = Flight::request()->data['YYYY'];
    $mm = Flight::request()->data['MM'];
    $dd = Flight::request()->data['DD'];


    $errMessage  = $status = ''; //set variable of status and error message null
    // format request data from front end user
    $name = test_input($name);
    $pass = test_input($pass);
    $pass2 = test_input($pass2);
    $email = test_input($email);


    //start check year, month and day if number
    if (!is_numeric($yyyy) or !is_numeric($mm) or !is_numeric($dd))
    {
        $errMessage = "Year or Month or Day should be digital";
        $status = '500';
    }
    else
    {
        $birthday = $yyyy.'-'.$mm.'-'.$dd; // format birthday date
    }

    //start check $name valid
    if (empty($name))
    {
        $errMessage = 'Name is required!';
        $status = '404';
    }
    else if (strlen($name) <= 2 || strlen($name) >= 20)
    {
        $errMessage = "Name should between 3- 20";
        $status = "500";
    }
    else if (!preg_match("/^[a-zA-Z ]*$/",$name))
    {
        $errMessage = "Name need letters and white space allowed";
        $status = '500';

    }
    //end check name

    //start check  passwords valid
    if (empty($pass))
    {
        $errMessage = 'Password is required!';
        $status = '404';
    }
    else if (strlen($pass) <= 5 || strlen($pass) >= 12)
    {
        $errMessage = "pass should between 6- 12";
        $status = "500";
    }
    else if (!preg_match("/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,15}$/",$pass))
    {
        $errMessage = "Password need letters and digital allowed";
        $status = '500';

    }
    else if ($pass <> $pass2)
    {
        $errMessage = "Two passwords are not match";
        $status = '500';
    }
    //end of check passwords


    //start check  email valid
    if (empty($email))
    {
        $errMessage = "Email is required";
        $status = '404';
    }
    else if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
    {
        $errMessage = "Invalid email format";
        $status = '500';
    }
    else
    {
        //check this email user if exist
        $select = "select * from users where email = '".$email."'";
        $result = mysqli_query(db(), $select);
        if ($row = mysqli_fetch_array($result))
        {
            // user exist, return error
            $errMessage = "Email address has already used!";
            $status = '500';
        }
        mysqli_close(db()); // close db

    }
    // end of check email

    if (($status == '404') || ($status == '500'))//status = '-1' means error array
    {
        $res = array(
            'status' => $status,
            'errMessage' => $errMessage,
        );

        return $res;
    }
    else //valid data success, insert db
    {
        $pass = password_hash($pass,PASSWORD_DEFAULT);
        $verifyToken = str_random(16);
        $insert = "insert into users (name, password, email, birthday, verify_token) value ('".$name."', '".$pass."', '".$email."', '".$birthday."', '".$verifyToken."')";
    //return $insert;
        if ($result = mysqli_query(db(), $insert))
        {
            // if insert success, then make JWT token and return it and data from db
            $payload=array('sub'=>'Flight','name'=>$email,'iat'=>1516239022);
            $jwt = new Jwt;
            $token = $jwt->getToken($payload);

            //return $verifyToken;

            $mail = new PHPMailer(true);

            try {
                //服务器配置
                $mail->CharSet ="UTF-8";                     //set email charset
                $mail->SMTPDebug = 0;                        // set debug input false
                $mail->isSMTP();                             // use SMTP
                $mail->Host = 'smtp.gmail.com';                // SMTP server
                $mail->SMTPAuth = true;                      // allow STMP auth
                $mail->Username = 'flighttest2020@gmail.com';                // SMTP username = email account
                $mail->Password = 'neo123$%^';             // SMTP password
                $mail->SMTPSecure = 'ssl';                    // allow TLS or ssl protocol
                $mail->Port = 465;                            // server port: 25 or 465, gmail use port 465

                $mail->setFrom('flighttest2020@gmail.com', 'Admin');  //sender's email address and name
                $mail->addAddress($email, $name);  // to email address and name
                //$mail->addAddress('ellen@example.com');  // more email address
                $mail->addReplyTo('flighttest2020@gmail.com', 'Admin'); //replay to email address, usually same as sender
                //$mail->addCC('cc@example.com');                    //cc
                //$mail->addBCC('bcc@example.com');                    //bcc

                //发送附件
                // $mail->addAttachment('../xy.zip');         // add file
                // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // send file and rename

                //Content
                $mail->isHTML(true);                                  // if use HTML format
                $mail->Subject = 'email confirm link' . time();
                $mail->Body    = '<h1><a href=www.freeshow.download/vm/?verifyToken='.$verifyToken.'>confirm</a> Please</h1>'. date('Y-m-d H:i:s');
                $mail->AltBody = 'www.freeshow.donwload/vm/?verifyToken='.$verifyToken;

                $mail->send();

                $errEmail = 'email send success!';
            } catch (Exception $e) {
                $errEmail =  'email send failed:'.$mail->ErrorInfo;
            }
            //$status = '201';
            $errMessage = 'email need confirm!';

            //return $verifyToken;

            $res = array(
                'status' => '200',
                'verifyToken' => $verifyToken,
                'token' => $token,
                'errEmail' => $errEmail,
                'errMessage' => $errMessage,
            );
            return $res;
        }
        else
        {
            // insert failed
            $res = array(
                'status' => '500',
                'errMessage' => 'data insert failed',
            );
            return $res;
        }
        mysqli_close(db()); // close db

    }
}