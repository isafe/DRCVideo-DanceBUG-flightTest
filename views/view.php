<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Material Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/style.default.css" id="theme-stylesheet">
    <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="/views/js/func.js"></script>

</head>

<body>
<div class="page login-page">
    <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
            <div class="row">
                <!-- Logo & Information Panel-->
                <div class="col-lg-6">
                    <div class="info d-flex align-items-center">
                        <div class="content">
                            <div class="logo">
                                <h1>WELCOME</h1>
                            </div>
                            <p></p>
                        </div>
                    </div>
                </div>
                <!-- Form Panel    -->
                <div class="col-lg-6 bg-white" >
                    <div class="form d-flex align-items-center" id="master">
                        <div class="content" >

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
<script>

    //console.log(localStorage.getItem("token"))
    let token = localStorage.getItem("token")
    if (!token)
    {
        alert("token is empty")
        window.location.href='/'
    }
    else
    {
        $.ajax({

            type: "GET",//http method
            dataType: "json",//data type
            url: "/view/"+localStorage.getItem("id") ,//url
            data: "",
            headers:{
                'Authorization': keyName+token
            },
            success: function (result) {
                console.log(result);//for debug


                if (result.status == 200) {
                    //alert("SUCCESS");
                    let content = `<div class="content"><div class="form-group">NAME: ${result.name}</div>
                                <div class="form-group">EMAIL: ${result.email}</div><div class="form-group">BIRTHDAY: ${result.birthday}</div>
                                <div class="form-group"><a href='/update'>update</a></div>
                                <div class="form-group"><button id="login" type="button" onclick="logout()" class="btn btn-primary">Logout</button>
                                </div>
                                </div>`
                    document.getElementById('master').innerHTML = content

                }

                else
                {
                    //alert("failed!  " + result.status + "  "  + result.errMessage);
                    //window.location.href='/'
                }

            },
            error : function() {
                alert("system error！");
            }
        });

    }




</script>
</html>
