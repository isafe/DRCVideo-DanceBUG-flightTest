<?php
require_once './helper/conn.php';
require_once './helper/jwt.php';

function update($id)
{
    $name = Flight::request()->data['name'];
    $pass = Flight::request()->data['pass'];
    $pass2 = Flight::request()->data['pass2'];
    $yyyy = Flight::request()->data['YYYY'];
    $mm = Flight::request()->data['MM'];
    $dd = Flight::request()->data['DD'];

    $errMessage  = $status  = ''; //set variable of status and error message null

    // format request data from front end user
    $name = test_input($name);
    $pass = test_input($pass);
    $pass2 = test_input($pass2);

    //check if request token JWT token success
    $auth = 'Authorization';
    $key = array_key_exists($auth, getheaders());

    //start check year, month and day if number
    if (!is_numeric($yyyy) or !is_numeric($mm) or !is_numeric($dd))
    {
        $errMessage = "Year or Month or Day should be digital";
        $status = '500';
    }
    else
    {
        $birthday = $yyyy.'-'.$mm.'-'.$dd; // format birthday date
    }


    if ($key)  // if token exist
    {
        //start check $name valid
        if (empty($name))
        {
            $errMessage = 'Name is required!';
            $status = '404';
        }
        else if (strlen($name) <= 2 || strlen($name) >= 20)
        {
            $errMessage = "Name should between 3- 20";
            $status = "500";
        }
        else if (!preg_match("/^[a-zA-Z ]*$/",$name))
        {
            $errMessage = "Name need letters and white space allowed";
            $status = '500';

        }
        //end check name

        //start check  passwords valid
        if (empty($pass))
        {
            $errMessage = 'Password is required!';
            $status = '404';
        }
        else if (strlen($pass) <= 5 || strlen($pass) >= 12)
        {
            $errMessage = "pass should between 6- 12";
            $status = "500";
        }
        else if (!preg_match("/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,15}$/",$pass))
        {
            $errMessage = "Password need letters and digital allowed";
            $status = '500';

        }
        else if ($pass <> $pass2)
        {
            $errMessage = "Two passwords are not match";
            $status = '500';
        }
        //end of check passwords


        if (($status == '404') || ($status == '500'))// error status  make error array
        {
            $res = array(
                'status' => $status,
                'errMessage' => $errMessage,
            );

            return $res;
        }
        else //valid data success, insert db
        {
            $pass = password_hash($pass, PASSWORD_DEFAULT); // encryption user's password
            $update = "update users set name = '" .$name. "', birthday = '" .$birthday. "', password = '" .$pass. "' where id = '".$id."'";
            if ($result = mysqli_query(db(), $update))
            {
                // update success and return message
                $res = array(
                    'status' => '200',
                    'errMessage' => "update success!",
                );
            }else
            {
                // update failed and return message
                $res = array(
                    'status' => '500',
                    'errMessage' => 'data update failed',
                );
            }
            mysqli_close(db()); // close db
            return $res;
        }
    }
    else // token empty
    {
        $status = "404";
        $errMessage = "token could not empty";
    }

    // make error message array
    $res = array(
        'errMessage' => $errMessage,
        'status' => $status
    );

    return $res;




}