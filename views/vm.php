<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bootstrap Material Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
      <link rel="stylesheet" href="/views/css/style.default.css" id="theme-stylesheet">
      <script src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
      <script src="/views/js/func.js"></script>
  </head>

  <body>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>WELCOME</h1>
                  </div>
                  <p></p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content" id="vm">


                    	      </div>
                    </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>
  <script>
      //let verifyToken = localStorage.getItem("verifyToken")
      //let token = localStorage.getItem("token")
      function GetRequest()
      {
          let strs = ''
          let url = location.search; //get letters after ?
          let theRequest = new Object();
          if(url.indexOf("?") != -1)
          {
              let str = url.substr(1);
              strs = str.split("&");
              for(let i = 0; i < strs.length; i ++)
              {
                  theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);
              }
          }
          return theRequest;
      }
      //create Request for recevie patterns from user's email confirm link
      let Request = new Object()
      Request = GetRequest()
      let verifyToken = Request['verifyToken']
      if (!verifyToken)
      {
          //document.getElementById("vm").innerHTML = "Waiting email confirm, please go to login"
          alert("Waiting email confirm, please go to login")
          window.location.href=('/')
      }
      else
      {
          console.log(verifyToken)
          let vm = `<div class="content"><button id="login" type="button" onclick="login1()" class="btn btn-primary">confirm</button>please click and confirm your email</div>`
          console.log(vm)
          document.getElementById("vm").innerHTML = vm
          function login1() {
                //console.log(localStorage.getItem("verifyToken"))
              $.ajax({

                  type: "GET",//http method
                  dataType: "json",//data type
                  url: "/verify",//url
                  data: {'verifyToken': verifyToken},
                  success: function (result) {
                      console.log(result);//for debug
                      if (result.status == 200) {
                          alert("SUCCESS");
                          window.location.href='/'
                          localStorage.setItem("email", result.email)
                          localStorage.setItem("token", result.token)
                          localStorage.setItem("birthday", result.birthday)
                          localStorage.setItem("id", result.id)
                          localStorage.setItem("name", result.name)

                      } else {
                          alert("failed!  " + result.status + "  " + result.errMessage);
                      }

                  },
                  error: function () {
                      alert("system error！");
                  }
              });

          }
      }


  </script>
</html>