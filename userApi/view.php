<?php
require_once './helper/conn.php';
require_once './helper/jwt.php';

function view($id)
{
    //check if request token JWT token success
    $auth = 'Authorization';
    $key = array_key_exists($auth, getheaders());
    $errMessage = $status = ''; //set variable of status and error message null


    if ($key) // if token exist
    {
        //search this id user if exist
        $select = "select * from users where id = '".$id."' ";
        $result = mysqli_query(db(), $select);
        if ($row = mysqli_fetch_array($result))
        {
            //if this id user exist, make JWT token from front end by header
            $header = getheaders()['Authorization'];
            $array = explode(" ", $header);
            $name = $array[0];
            $tokenHeader = $array[1];

            //make JWT token from databbase
            $payload = array('sub'=>$name,'name'=>$row['email'],'iat'=>1516239022);
            $jwt = new Jwt;
            $tokenData = $jwt->getToken($payload); //token from back end by database


            //compare front end JWT token == back end JWT token
            if ($tokenData == $tokenHeader)
            {
                //JWT tokens equaled, then make user data to array and return to front end
                $res = array(
                    'name' => $row['name'],
                    'email' =>$row['email'],
                    'birthday' => $row['birthday'],
                    'status' => '200',
                );

                return $res;
            }
            else
            {
                // if JWT tokens not equaled, then return error message
                $status = '500';
                $errMessage = "tokens are not matched";
            }

            mysqli_close(db()); //close db

        }
        else // this id user not exist in DB
        {
            $status = '404';
            $errMessage = "this user does not exist";
        }

    }
    else
    {
        //front end JWT token is empty
        $status = "404";
        $errMessage = "JWT token could not empty";
    }

    $res = array (
        'status' => $status,
        'errMessage' => $errMessage,
    );

    return $res;





}