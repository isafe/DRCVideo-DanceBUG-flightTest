<?php
require_once './helper/conn.php';
require_once './helper/jwt.php';



// verify function for verify email
function verify()
{
    $verifyToken = Flight::request()->query['verifyToken'];
    $status = $errMessage = '';//set variable of status and error message null

    if (empty($verifyToken)) // check verifyToken if empty
    {
        //empty, then return error message
        $status = '404';
        $errMessage = "token is empty";
    }
    else
    {
        //verify token is exist, select this verify token's user if exist
        $select = "select * from users where verify_token = '".$verifyToken."'";
        //return $select;
        $result = mysqli_query(db(), $select);
        if ($row = mysqli_fetch_array($result))
        {
            // user exist, update this user's verified status to 1, means already verified
            $id = $row['id'];
            $email = $row['email'];
            $name = $row['name'];
            $birthday = $row["birthday"];

            $update = "update users set verified = 1 where  verify_token = '".$verifyToken."'";
            //return $update;
            if ($result = mysqli_query(db(), $update))
            {
                //make JWT token
                $payload = array('sub'=>'Flight','name'=>$row['email'],'iat'=>1516239022);
                $jwt = new Jwt;
                $tokenData = $jwt->getToken($payload);

                //make user's array and return to front end
                $res = array(
                    'status' => '200',
                    'id' => $id,
                    'email' => $email,
                    'name' => $name,
                    'birthday' => $birthday,
                    'token' => $tokenData,

                );

                return $res;

                mysqli_close(db());

            }
            else // update failed
            {
                $status = '500';
                $errMessage = "update failed";
            }
        }
        else // this verifyToken's user not exist
        {
            $status = "404";
            $errMessage = "this user does not exist";
        }

    }

    $res = array(
        'status' => $status,
        'errMessage' => $errMessage,
    );


    return $res;
}